import unittest
import vegetatiemonitor2waqua as v2w
from pathlib import Path
import logging
import geopandas as gpd

logger = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)


class test_main(unittest.TestCase):

    def test_001(self):
        """Test small test case"""

        outputdir = Path('test_001')
        outputdir.mkdir(exist_ok=True)

        v = v2w.vegetatiemonitor2waqua()

        veg_monitor_file = r'../data/vegetatiemonitor/opbossen/Millingerwaard/landuse_2019-01-01_2020-01-01.remapped.tif'

        conversietabel = {
            1: 0,
            2: 114,
            3: 1981,
            4: 1982,
            5: 1983,
            6: 1984,
        }

        v.load_vegetatiemonitor(veg_monitor_file=veg_monitor_file, conversietabel=conversietabel)

        baselinegdb = r'../data/baseline/rijn-beno18_5-v1/baseline.gdb'
        v.load_baseline_ecotopen(baselinegdb=baselinegdb)

        union_export_file = outputdir / "ecotopen_vlakken.shp"
        statistics_export_file = outputdir / 'statistics.xlsx'
        keep_from_reference = [131, 132, 133, 611, 612, 613, 614, 615, 616, 617, 618, 619, 620, 631, 632, 633, 634, 635,
                              636, 637, 638, 639, 640, 641, 642, 643, 644, 651, 652, 653, 654, 655, 656, 657, 658, 659,
                              660, 661, 662, 671, 672, 673]
        v.process(union_export_file=union_export_file, statistics_export_file=statistics_export_file,
                  keep_from_reference=keep_from_reference)

        # Compare outcome with reference
        reference_file = 'reference/ecotopen_vlakken.shp'
        reference = gpd.read_file(reference_file)
        output = gpd.read_file(union_export_file)
        self.assertTrue(output.equals(reference))

    def test_002(self):
        """Test large test case"""

        outputdir = Path('test_002')
        outputdir.mkdir(exist_ok=True)

        v = v2w.vegetatiemonitor2waqua()

        veg_monitor_file = r'../data/vegetatiemonitor/opbossen/Hele beheergebied/yearly-classified-geotiffs_classified-image-2019.tif'

        conversietabel = {
            1: 0,
            2: 114,
            3: 1981,
            4: 1982,
            5: 1983,
            6: 1984,
        }

        v.load_vegetatiemonitor(veg_monitor_file=veg_monitor_file, conversietabel=conversietabel)

        baselinegdb = r'../data/baseline/rijn-beno18_5-v1/baseline.gdb'
        v.load_baseline_ecotopen(baselinegdb=baselinegdb)

        union_export_file = outputdir / "ecotopen_vlakken.shp"
        statistics_export_file = outputdir / 'statistics.xlsx'
        keep_from_reference = [131, 132, 133, 611, 612, 613, 614, 615, 616, 617, 618, 619, 620, 631, 632, 633, 634, 635,
                              636, 637, 638, 639, 640, 641, 642, 643, 644, 651, 652, 653, 654, 655, 656, 657, 658, 659,
                              660, 661, 662, 671, 672, 673]
        v.process(union_export_file=union_export_file, statistics_export_file=statistics_export_file,
                  keep_from_reference=keep_from_reference)

    def test_003(self):
        """Test conversion to WAQUA"""

        outputdir = Path('test_003')
        outputdir.mkdir(exist_ok=True)

        v = v2w.vegetatiemonitor2waqua()

        v.baswaqexe = r'../data/Baswaq.exe'

        union_export_file = Path('test_002') / "ecotopen_vlakken.shp"
        v.load_export(union_export_file)

        roostergdb = r'../data/baseline/rijn-beno18_5-v1/modellen/waqua/beno18_5-v1/rooster.gdb'
        baswaq_invoer_directory = r'../data/baseline/rijn-beno18_5-v1/modellen/waqua/beno18_5-v1/invoer'
        run_directory = outputdir / 'baswaq'
        v.convert_to_waqua(roostergdb=roostergdb, baswaq_invoer_directory=baswaq_invoer_directory,
                           run_directory=run_directory, casename='TEST')

    def test_004(self):
        """Test config reader"""
        configuration = {
            "vegetatiemonitor": {
                "tif": "../data/vegetatiemonitor/opbossen/Millingerwaard/landuse_2019-01-01_2020-01-01.remapped.tif",
                "conversietabel": {
                    "1": 0,
                    "2": 114,
                    "3": 1981,
                    "4": 1982,
                    "5": 1983,
                    "6": 1984
                }
            },
            "reference": {
                "baselinegdb": "../data/baseline/rijn-beno18_5-v1/baseline.gdb",
                "roostergdb": "../data/baseline/rijn-beno18_5-v1/modellen/waqua/beno18_5-v1/rooster.gdb",
                "baswaq_invoer": "../data/baseline/rijn-beno18_5-v1/modellen/waqua/beno18_5-v1/invoer",
                "keep_from_reference": [131, 132, 133, 611, 612, 613, 614, 615, 616, 617, 618, 619, 620, 631, 632, 633,
                                      634, 635, 636, 637, 638, 639, 640, 641, 642, 643, 644, 651, 652, 653, 654, 655,
                                      656, 657, 658, 659, 660, 661, 662, 671, 672, 673]
            },
            "settings": {
                "outputfolder": "test_004",
                "name": "test_004",
                "baswaqexe": "c:/Projecten/Vegetatiemonitor/Baswaq.exe"
            }
        }

        v2w.vegetatiemonitor2waqua(configuration=configuration)

    def test_005(self):
        """Test convert original ecotopen to WAQUA"""
        outputdir = Path('test_005')
        outputdir.mkdir(exist_ok=True)

        v = v2w.vegetatiemonitor2waqua()
        v.baswaqexe = r'../data/Baswaq.exe'

        baselinegdb = r'../data/baseline/rijn-beno18_5-v1/baseline.gdb'
        v.load_baseline_ecotopen(baselinegdb=baselinegdb)

        v.union = v.baseline_ruwheidsvlakken
        v.union.to_crs(v.crs_output, inplace=True)
        v.union.rename({'ecotopen_ori': 'ecotopen'}, axis=1, inplace=True)

        roostergdb = r'../data/baseline/rijn-beno18_5-v1/modellen/waqua/beno18_5-v1/rooster.gdb'
        baswaq_invoer_directory = r'../data/baseline/rijn-beno18_5-v1/modellen/waqua/beno18_5-v1/invoer'
        v.convert_to_waqua(roostergdb=roostergdb, baswaq_invoer_directory=baswaq_invoer_directory,
                           run_directory=outputdir / 'baswaq', casename='005')


if __name__ == '__main__':
    unittest.main()
