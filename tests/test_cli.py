import unittest

from click.testing import CliRunner
from vegetatiemonitor2waqua import cli


class test_cli(unittest.TestCase):

    def test_command_line_interface(self):
        """Test the CLI."""
        runner = CliRunner()

        # Test simple help call
        help_result = runner.invoke(cli.main, ['--help'])
        assert help_result.exit_code == 0
        assert '--help  Show this message and exit.' in help_result.output

        # Test yaml file
        yamlinput = r'../data/test.yaml'
        result = runner.invoke(cli.main, yamlinput)
        assert result.exit_code == 0

if __name__ == '__main__':
    unittest.main()
