#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = ['Click>=6.0', 'geopandas>=0.6.1', 'rasterio', 'pyyaml', 'pyproj', 'openpyxl']

setup_requirements = [ ]

test_requirements = [ ]

dev_requirements = ['ipykernel>=5.1.4', 'matplotlib']

setup(
    author="Jurjen de Jong",
    author_email='jurjen.dejong@deltares.nl',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.7',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3.6',
        'Programming Language :: Python :: 3.7',
    ],
    description="A module to convert vegetatiemonitor to WAQUA",
    entry_points={
        'console_scripts': [
            'vegetatiemonitor2waqua=vegetatiemonitor2waqua.cli:main',
        ],
    },
    install_requires=requirements,
    license="MIT license",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='vegetatiemonitor2waqua',
    name='vegetatiemonitor2waqua',
    packages=find_packages(include=['vegetatiemonitor2waqua']),
    setup_requires=setup_requirements,
    test_suite='tests',
    tests_require=test_requirements,
    url='https://gitlab.com/deltares/vegetatiemonitor2waqua',
    version='0.1.0',
    zip_safe=False,
)
