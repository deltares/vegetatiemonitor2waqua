# Start with docker image from anaconda
FROM continuumio/miniconda3:4.6.14

# Install conda stuff first
RUN conda install geopandas rasterio 
# Then install rest via pip
RUN pip install pyyaml pyproj openpyxl

ADD . /vegetatiemonitor2waqua
WORKDIR /vegetatiemonitor2waqua

# Install the application
RUN pip install -e .

RUN vegetatiemonitor2waqua data/test.yaml