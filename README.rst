====================
Vegatiemonitor2WAQUA
====================

Tool for converting vegetatiemonitor output rasters to roughness input for a WAQUA model. To make sure it works

After installing the tool to your python installation it can be run by insert the yaml-configuration-file:

	vegetatiemonitor2waqua "path/to/yamlfile.yaml"

This yaml-file has the following structure:

.. code-block:: yaml

    vegetatiemonitor:
        tif: input\vegetatiemonitor\classified-image-2017-ahn3-ecotope-filtering-6-class.tif
        conversietabel: {  # Translating the classes in the tiff to the baseline-classes
                    0: 0,
                    1: 0,
                    2: 114,
                    3: 1981,
                    4: 1982,
                    5: 1983,
                    6: 1984,
            }
    reference:
        baselinegdb:  input/baseline-rijn-j17_5-v1/baseline.gdb
        roostergdb: input/baseline-rijn-j17_5-v1/modellen/waqua/j17_5-v1/rooster.gdb
        baswaq_invoer: input/baseline-rijn-j17_5-v1/modellen/waqua/j17_5-v1/invoer
        keep_from_reference: [131, 132, 133]  # These codes should not be overriden by vegetatiemonitor-data
    settings:
        outputfolder: output/j17_JK_2017
        statistics_file: statistics.xlsx  # Optional, defaults to not exporting
        name: j17_JK_2017  # Name (or ID) to be used in the WAQUA-files
        baswaqexe: c:\Projecten\Vegetatiemonitor\Baswaq.exe  # Optional, defaults to Program Files
        crs_compute: 4326  # Optional, defaults to 4326
        crs_output: 28992  # Optional, defaults to 28992
