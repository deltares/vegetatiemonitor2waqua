"""Top-level package for Vegetatiemonitor2WAQUA."""

__author__ = """Jurjen de Jong"""
__email__ = "Jurjen.deJong@deltares.nl"
__version__ = "0.0.1"

from .main import vegetatiemonitor2waqua
