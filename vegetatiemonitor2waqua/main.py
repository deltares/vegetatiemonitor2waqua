import logging
# import fiona
import geopandas as gpd
from shapely.geometry import shape
from rasterio.features import shapes
import rasterio
from typing import Union, Dict, Sequence
from pathlib import Path
import shutil
import subprocess

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)

waqini_invoer = r"""
1
{casename}
 2
           1
 11
.
 21
{rgf_file_name}
 41
invoer\rrb_v.gen
 42
invoer\rrb_v.asc
 115
invoer\ruwpunt_p.gen
 116
invoer\ruwpunt_p.asc
 111
invoer\ruwlijn_l.gen
 112
invoer\ruwlijn_l.asc
 121
invoer\ruwvlak-u.asc
 122
invoer\ruwvlak-v.asc
 132
1
"""


class vegetatiemonitor2waqua:

    baswaqexe = r"c:\Program Files (x86)\Deltares\Baseline 5\Baswaq.exe"
    crs_compute = 4326
    crs_output = 28992

    def __init__(self, configuration=None):

        self.vegmon = None
        self.union = None
        self.baseline_ruwheidsvlakken = None

        if configuration is not None:
            self.run_configuration(configuration)

    def __repr__(self):
        return """Class Vegetatiemonitor2WAQUA"""

    def run_configuration(self, configuration):
        # Perform all actions

        output = Path(configuration['settings']['outputfolder'])
        output.mkdir(exist_ok=True)

        for setting in ['baswaqexe', 'crs_compute', 'crs_output']:
            if setting in configuration['settings']:
                setattr(self, setting, configuration['settings'][setting])

        veg_monitor_file = configuration['vegetatiemonitor']['tif']

        conversietabel = configuration['vegetatiemonitor']['conversietabel']
        conversietabel = {int(k): int(v) for k, v in conversietabel.items()}

        vegmonitor_shapefile = output / configuration['settings'].get('vegmonitor_shapefile', "vegmonitor_shapefile.shp")
        self.load_vegetatiemonitor(veg_monitor_file=veg_monitor_file, conversietabel=conversietabel,
                                   export_to_shapefile=vegmonitor_shapefile)

        baselinegdb = configuration['reference']['baselinegdb']
        self.load_baseline_ecotopen(baselinegdb=baselinegdb)

        union_export_file = output / configuration['settings'].get('outputfilename', "ecotopen_vlakken.shp")
        statistics_export_file = output / configuration['settings'].get('statistics_file', 'statistics.xlsx')
        keep_from_reference = configuration['settings'].get('keep_from_reference', [])
        self.process(union_export_file=union_export_file, statistics_export_file=statistics_export_file,
                     keep_from_reference=keep_from_reference)

        roostergdb = configuration['reference']['roostergdb']
        baswaq_invoer = configuration['reference']['baswaq_invoer']
        casename = configuration['settings']["name"]

        self.convert_to_waqua(roostergdb=roostergdb, baswaq_invoer_directory=baswaq_invoer,
                              run_directory=output / 'baswaq', casename=casename)

    def load_vegetatiemonitor(self, veg_monitor_file: Union[Path, str],
                              conversietabel: Dict[int, int],
                              export_to_shapefile: Union[Path, str] = None) -> gpd.geodataframe:
        """
        Load tif of vegetatiemonitor
        Translate codes using conversietable
        Write to shapefile

        :param veg_monitor_file: filepath (Path or str)
        :param conversietabel: Dict of before and after values. Convert to zero to have it removed from the dataframe
        :param export_to_shapefile: filepath to export vegetatiemonitor as shapefile
        :return:
        """
        logger.info('Loading input from Vegetatiemonitor')

        # Read tiff file
        vegmon = self.read_tiff_to_geopandas(veg_monitor_file)

        # Replace values with conversiontable
        vegmon.replace({'vegmonitor': conversietabel}, inplace=True)

        # Remove any row that now has a value of zero
        ii = vegmon['vegmonitor'] == 0
        vegmon = vegmon[~ii]

        # Convert to Dutch coordinate system
        vegmon = vegmon.to_crs(epsg=self.crs_compute)

        self.vegmon = vegmon

        if export_to_shapefile is not None:
            vegmon.to_file(export_to_shapefile)

        return self.vegmon

    def load_baseline_ecotopen(self, baselinegdb: Union[Path, str],
                               baseline_featuretype='ruwheid_vlakken') -> gpd.geodataframe:
        """
        Load ecotopen from Baseline
        Convert coordinatesystem

        :param baselinegdb:
        :param baseline_featuretype: usually
        :return:
        """
        logger.info('Loading input from Baseline')

        # layers = fiona.listlayers(baselinegdb)
        baseline_ruwheidsvlakken = gpd.read_file(baselinegdb, layer=baseline_featuretype)
        baseline_ruwheidsvlakken.drop(['KENMERK', 'Shape_Length', 'Shape_Area'], axis=1, inplace=True)
        baseline_ruwheidsvlakken.rename({'RUWHEIDSCODE': 'ecotopen_ori'}, axis=1, inplace=True)

        baseline_ruwheidsvlakken = baseline_ruwheidsvlakken.to_crs(epsg=self.crs_compute)

        self.baseline_ruwheidsvlakken = baseline_ruwheidsvlakken
        return self.baseline_ruwheidsvlakken

    def process(self, union_export_file: Union[Path, str],
                statistics_export_file: Union[Path, str],
                keep_from_reference: Sequence) -> gpd.geodataframe:
        """
        Combine vegetatiemonitor and baseline-eco-topen
        Create column 'ecotopen' with the ecotopen to be applied
        Write to shapefile and excel

        :param union_export_file: filepath for shapefile
        :param statistics_export_file: filepath for excelfile
        :param keep_from_reference: Polygons with these codes must kept from baseline-ecotopen
        """

        logger.info('Combining vegetatiemonitor and baseline-data')

        # Very slow. Maybe this can be optimised.
        union = gpd.overlay(self.baseline_ruwheidsvlakken, self.vegmon,
                            how='union')  # TODO: Test change to how='identity'

        union = union.to_crs(epsg=self.crs_output)

        union['area'] = union.area

        if statistics_export_file is not None:
            ii = union['vegmonitor'] > 1
            statistics = union[ii].groupby(['ecotopen_ori', 'vegmonitor'])['area'].sum()
            statistics = statistics.unstack(level=0)

            statistics.to_excel(statistics_export_file)

        # Alle polygonen zonder referentie vallen buiten het interessegebied
        ii = union['ecotopen_ori'].isna()
        union = union.loc[~ii]

        # De basis is de vegetatiemonitor
        union['ecotopen'] = union['vegmonitor']

        # Dit wordt overschreven met zomerbed
        ii = union['ecotopen_ori'].isin(keep_from_reference)
        union.loc[ii, 'ecotopen'] = union.loc[ii, 'ecotopen_ori']

        # Vervolgens aanvullen met ecotopen_ruwheid waar nog waarden ontbreken
        ii = union['ecotopen'].isna()
        union.loc[ii, 'ecotopen'] = union.loc[ii, 'ecotopen_ori']

        self.union = union

        if union_export_file is not None:
            union.to_file(union_export_file)

        logger.info('  Combining finished')
        return self.union

    def load_export(self, ecotopenfile: Union[Path, str]) -> None:
        """
        Loading an earlier export of the output of self.process()

        :param ecotopenfile: filepath to shapefile
        :return:
        """

        logger.info('Loading earlier export')
        self.union = gpd.read_file(ecotopenfile)

    def convert_to_waqua(self, roostergdb: Union[Path, str], baswaq_invoer_directory: Union[Path, str],
                         run_directory: Union[Path, str], casename: str) -> None:
        """
        Convert combined ecotopen to baseline

        :param roostergdb: filepath to rooster.gdb
        :param baswaq_invoer_directory: filepath to waqua/model/invoer-directory
        :param run_directory: directory to run baswaq
        :param casename: WAQUA-RUNID for output files
        :return:
        """

        # Create baswaq folder
        logger.info('Creating and copying default baswaq files')
        baswaq_invoer_directory = Path(baswaq_invoer_directory)

        run_directory = Path(run_directory)
        run_directory.mkdir(exist_ok=True)

        run_dir_invoer = run_directory / 'invoer'
        run_dir_invoer.mkdir(exist_ok=True)

        new_folders = ['randen', 'ruwheid', 'schotjes']
        for folder in new_folders:
            (run_directory / folder).mkdir(exist_ok=True)

        copy_files = ['rrb_v.asc', 'rrb_v.gen', 'ruwlijn_l.asc', 'ruwlijn_l.gen', 'ruwpunt_p.asc', 'ruwpunt_p.gen']
        for file in copy_files:
            shutil.copyfile(baswaq_invoer_directory / file, run_dir_invoer / file)

        # Find first grid file in parent directory
        rgf_file: Path = list(baswaq_invoer_directory.parent.glob('*.rgf'))[0]
        rgf_file_name = rgf_file.name
        shutil.copyfile(rgf_file, run_directory / rgf_file_name)

        # Create input file
        with open(run_directory / 'baswaq.inv', 'w') as fout:
            fout.write(waqini_invoer.format(rgf_file_name=rgf_file_name,
                                            casename=casename))

        def combine_ecotopes_with_grid(ecotopes, grid):
            """
            Merge ecotopen and grid geopandas

            :param ecotopes:
            :param grid:
            :return:
            """
            # Very slow. Possible ways to speed up:
            # (1) Do smaller actions. Per ruwheidscode (tested: not good), or per range of gridcells
            # (2) Use external gdal routines?
            logger.info('  Combine grid with ecotopen')
            union_ecotopes_grid = gpd.overlay(ecotopes, grid, how='intersection')
            logger.info('  Combining finished. Exporting for use in baswaq')

            # create column to dissolve on
            union_ecotopes_grid['dissolve'] = \
                union_ecotopes_grid[['ecotopen', 'M_COORD', 'N_COORD']].astype(str).agg('_'.join, axis=1)
            union_ecotopes_grid_dissolve = union_ecotopes_grid.dissolve('dissolve', aggfunc={'Shape_Area': 'first'})

            # Compute area of each cell, using the property 'Shape_Area' of the loaded grid cells
            union_ecotopes_grid_dissolve['Partial_area'] = \
                union_ecotopes_grid_dissolve.area / union_ecotopes_grid_dissolve['Shape_Area']

            # Drop very small cells
            ii = union_ecotopes_grid_dissolve['Partial_area'] >= 0.000001
            union_ecotopes_grid_dissolve = union_ecotopes_grid_dissolve[ii]

            # Split dissolve column (now index) back to 3 columns
            union_ecotopes_grid_dissolve.index = union_ecotopes_grid_dissolve.index.str.split('_', expand=True)
            union_ecotopes_grid_dissolve.index = union_ecotopes_grid_dissolve.index.set_names(
                ['ecotopen', 'M_COORD', 'N_COORD'])
            union_ecotopes_grid_dissolve.reset_index(inplace=True)

            # Sorting for convenience only
            union_ecotopes_grid_dissolve.sort_values(['ecotopen', 'M_COORD', 'N_COORD'], axis=0, inplace=True)

            # Select columns and save to file
            union_ecotopes_grid_dissolve = union_ecotopes_grid_dissolve[
                ['M_COORD', 'N_COORD', 'ecotopen', 'Partial_area']]
            union_ecotopes_grid_dissolve['ecotopen'] = union_ecotopes_grid_dissolve['ecotopen'].astype(float).astype(
                int)  # Could possibly also be done earlier
            return union_ecotopes_grid_dissolve

        # Load roostervlakken van rooster.gdb
        # layers = fiona.listlayers(roostergdb)
        logger.info('Read rooster.gdb')
        uvlakken = gpd.read_file(roostergdb, layer='rooster_u_vlakken')
        uvlakken.drop(['ID', 'Shape_Length'], axis=1, inplace=True)
        vvlakken = gpd.read_file(roostergdb, layer='rooster_v_vlakken')
        vvlakken.drop(['ID', 'Shape_Length'], axis=1, inplace=True)

        logger.info('Projecting to u-vlakken')
        union_u_dissolve = combine_ecotopes_with_grid(ecotopes=self.union, grid=uvlakken)
        union_u_dissolve.to_csv(run_dir_invoer / 'ruwvlak-u.asc', float_format='%.6f', index=False, header=False)

        logger.info('Projecting to v-vlakken')
        union_v_dissolve = combine_ecotopes_with_grid(ecotopes=self.union, grid=vvlakken)
        union_v_dissolve.to_csv(run_dir_invoer / 'ruwvlak-v.asc', float_format='%.6f', index=False, header=False)

        logger.info('Running baswaq')
        stdin = open(run_directory / 'baswaq.inv')
        stdout = open(run_directory / 'baswaq.log', 'w')

        subprocess.run([self.baswaqexe],
                       cwd=str(run_directory.absolute()),
                       stdin=stdin, stdout=stdout)

    @staticmethod
    def read_tiff_to_geopandas(veg_monitor_file: Union[Path, str]) -> gpd.geodataframe:
        """
        Read tiff file to geopandas

        :param veg_monitor_file: tiff-file
        :return: GeoDataFrame
        """
        with rasterio.open(veg_monitor_file, driver='GTiff', ) as src:
            image = src.read(1)

            gdf = gpd.GeoDataFrame(
                [{'vegmonitor': v, 'geometry': s} for s, v in shapes(image, transform=src.transform)]
            )
            gdf['geometry'] = gdf['geometry'].apply(shape)
            gdf.set_crs(src.crs.get('init'), inplace=True)
        return gdf
