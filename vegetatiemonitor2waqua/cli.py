import click
import logging
import yaml
from vegetatiemonitor2waqua.main import vegetatiemonitor2waqua

logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%H:%M:%S',
                    handlers=[
                        logging.FileHandler('vegetatiemonitor2waqua.log', mode='a'),
                        logging.StreamHandler()
                    ])

logger = logging.getLogger(__name__)


@click.command()
@click.argument('yamlfile', type=click.File('rb'))
def main(yamlfile):
    logger.info(f'Loading configuration: {yamlfile.name}')
    config = yaml.safe_load(yamlfile.read())
    logger.info(config)
    logger.info('Running configuration')
    vegetatiemonitor2waqua(configuration=config)


if __name__ == '__main__':
    main()
